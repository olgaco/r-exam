#Good luck , good morning!

##########################  Q1   ############################################

check<- read.csv('loan_data.csv', stringsAsFactors = FALSE)
loan.df<-read.csv('loan_data.csv', stringsAsFactors = FALSE)
str(check)
str(loan.df)

#credit.policy as factor (1= meets , 0= Not meets)
loan.df$credit.policy<- as.factor(loan.df$credit.policy)


#not.fully.paid as factor (0= fully paid, 1= not fully paid)
loan.df$not.fully.paid<- as.factor(loan.df$not.fully.paid)

# pub.rec as factor
loan.df$pub.rec <- as.factor(loan.df$pub.rec)


summary(loan.df$inq.last.6mths)

#Check missing values
any(is.na(loan.df))
#false - no NA values


#############################  Q2   #######################################


install.packages("ggplot2")
library(ggplot2)

#1 

#Use histograms to review the effect of the int.rate on the target attribute (not.fully.paid)

P1 <- ggplot(loan.df, aes(x=loan.df$int.rate, fill = loan.df$not.fully.paid)) + geom_histogram(color= 'blue')
P1 <- P1 + ggtitle('A review the effect of the int.rate on the target attribute (not.fully.paid)')

 
#conclusion - int ration between 0.10-0.15 has the higher return chance 


#2 

P2<- ggplot(loan.df, aes(x=purpose))
P2<-P2 + geom_bar(color= 'blue', fill= 'light blue')
#add another layer and parameter to the graph
P2<-P2 +geom_bar(aes(fill = not.fully.paid))
P2<-P2 +geom_bar(aes(fill = not.fully.paid), position = 'fill')
P2 <- P2 + ggtitle('A review the effect of the purpose on the target attribute (not.fully.paid)')


#conclusion- the porpuse "debt_consolidation" is the porpuse with the highst sate of fully pais and not fully paid


#3

#scatter chart
P3<- ggplot(loan.df,aes(x = fico , y=int.rate))
P3<- P3 +geom_point(alpha = 0.2)
# add another layer to limit the lenght
P3<- P3+ ggtitle('A review the effect of the FICO on the int.rate')



#4 - ?????




########################################   Q3    ###########################################

install.packages('caTools')
library(caTools)
install.packages('rpart')
library(rpart)
install.packages('rpart.plot')
library(rpart.plot)

sample <- sample.split(loan.df,0.7)

train <- loan.df[sample == T,]
test <- loan.df[sample == F,]

#Derive the tree model 
tree <- rpart(not.fully.paid ~ .,method = 'class' ,data = train)

prp(tree)


## No tree was produce.


########################################   Q4    ###########################################

#1
str(loan.df)

#Compute the logistic regression model 
model <- glm(not.fully.paid ~ ., family = binomial(link = 'logit'), data = train)

summary(model)
step.model <- step(model)

summary(step.model)

predictecProb <- predict(model, newdata = test, type = 'response')

#### Error in model.frame.default(Terms, newdata, na.action = na.action, xlev = object$xlevels) : 
####factor pub.rec has new levels 4

#2
newdf <- loan.df
str(newdf)
newdf$pub.rec <- NULL

sample <- sample.split(newdf,0.7)

train.new <- loan.df[sample == T,]
test.new <- loan.df[sample == F,]

model.new <- glm(not.fully.paid ~ ., family = binomial(link = 'logit'), data = train.new)

summary(model.new)

predictecProb <- predict(model.new, newdata = test.new, type = 'response')

predictedValues <- ifelse(predictecProb > 0.5, 1, 0)

#3
cm <- table(test.new$not.fully.paid, predictecProb>0.5)

#    FALSE TRUE
#0  2465   18
#1   450   14

install.packages('pROC')
library(pROC)

#4

rocCurve <- roc(test.new$not.fully.paid, predictecProb, levels = c("0","1"))
plot(rocCurve, col = "blue", main = "Roc Chart")



